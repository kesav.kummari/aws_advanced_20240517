#### Session Video
```
Part-1 :

https://drive.google.com/file/d/1YCKrVkSgvfnv5Kj0ulbE7kz_CHs_ujkF/view?usp=sharing

Part-2 :

https://drive.google.com/file/d/1CGuq8cEWHha2AsPR3dL_fAMXaJupy59r/view?usp=sharing
```

#### Setup Disaster Recovery :

```
1. EC2 Instances

    - Windows :
        - 30 GB 
    - Linux   :
        - 8 GB

2. RDS Databases
    - MySQL :
        - 20 GB 
    - Oracle
    - MSSQL

```