#### Session Video
```

```

#### AWS Services

```
Amazon Athena
Amazon OpenSearch Service
Amazon QuickSight
Amazon EMR

Amazon Kinesis Data Firehose
Amazon Kinesis Data Streams


Amazon EventBridge (Amazon CloudWatch Events)

Amazon AppFlow : https://aws.amazon.com/appflow/

AWS App Runner :
AWS App Runner is a fully managed container application service offered by Amazon Web Services. Launched in May 2021, it is designed to simplify the process of building, deploying, and scaling containerized applications for developers.

Amazon EC2

Amazon EC2 Auto Scaling

EC2 Image Builder :
    https://aws.amazon.com/image-builder/

AWS Elastic Beanstalk

AWS Serverless Application Repository :
https://aws.amazon.com/serverless/serverlessrepo/

```
