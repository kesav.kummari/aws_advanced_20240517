#### Session Video
```
https://drive.google.com/file/d/1SXac_V7yUFapUNIFguQyuJNLR4PJ44yZ/view?usp=sharing
```

#### AWS Services

```
1. AWS Resource Groups & 
2. Tag Editors
3. AWS Glue  
4. Business Analytics Service - Amazon QuickSight
5. Amazon SageMaker (Build, train, and deploy machine learning models at scale)
6. Amazon Bedrock : The easiest way to build and scale generative AI applications with foundation models
7. Amazon OpenSearch Service : Securely unlock real-time search, monitoring, and analysis of business and operational data
8. Amazon EMR : Easily run and scale Apache Spark, Hive, Presto, and other big data workloads

```
