#### Session Video
```
https://drive.google.com/file/d/1tfoUpCORqtqmOztlxASNPR-WOE6_q3HE/view?usp=sharing
```

How to create User in AWS?
```
    - IAM: 
        - User : jessi : 
        https://cloudbinary-awsdevops.signin.aws.amazon.com/console
        jessi

                    - AWS Management Console : GUI 
                    - Programmatic access    : CLI , SDK(Python, Go, Java etc...) 
        - Set Permissions:
            - Permissions options :
                1. Add user to group
                2. Copy permissions
                3. Attach policies directly
```


```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ec2:*",
                "ec2:DescribeInstances",
                "ec2:StartInstances",
                "ec2:StopInstances",
                "s3:*",
                "rds:*",
                "dynamodb:*",
                "route53:*",
                "cloudfront:*",
                "acm:ListCertificates",
                "acm:DescribeCertificate"
            ],
            "Resource": "*",
            "Condition": {
                "StringEquals": {
                    "aws:RequestedRegion": [
                        "us-east-1",
                        "ap-south-2"
                    ]
                }
            }
        }
    ]
}
```


How to create Service User in AWS?

```


Problem Statement:
    - Identify the Service Users, Roles, Policies were used by when? By Whom?
```