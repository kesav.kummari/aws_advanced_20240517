#### Session Video
```
https://drive.google.com/file/d/1mA9dp-6cXNw4WbNHF-32eSVB72sGwhOm/view?usp=sharing
```

#### Amazon Elastic Kubernetes Service

```
PaaS : K8s Setup 

Elastic Kubernetes Service (Amazon EKS):

    - Fully managed Kubernetes control plane
    - Amazon EKS is a managed service that makes it easy for you to use Kubernetes on AWS without needing to install and operate your own Kubernetes control plane.


    - Amazon EKS exposes a Kubernetes API endpoint.
    - Your existing Kubernetes tooling can connect directly to EKS managed control plane. 
    - Worker nodes run as EC2 instances in your account.

Setup Cluster:

EKS --> Clusters : Register cluster : Add or Register Existing Cluster from any vendor

Create Cluster in AWS :

EKS --> Clusters : Create EKS cluster

Step 1 : Configure cluster:

Name : c3ops-dev

Kubernetes version: 1.29

https://docs.aws.amazon.com/eks/latest/userguide/kubernetes-versions.html

Cluster service role : c3ops-eksClusterRole

https://docs.aws.amazon.com/eks/latest/userguide/service_IAM_role.html#create-service-role

Step 2 : Specify networking

Step 3 : Configure observability

Step 4 : Select add-ons

Step 5 : Configure selected add-ons settings

Step 6 : Review and create



```

#### Amazon Elastic Container Registry

```
1. Share and deploy container software, publicly or privately

2. Amazon Elastic Container Registry (ECR) is a fully managed container registry that makes it easy to store, manage, share and deploy your container images and artifacts anywhere.

```