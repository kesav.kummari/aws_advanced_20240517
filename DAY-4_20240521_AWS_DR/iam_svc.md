#### Session Video
```
https://drive.google.com/file/d/1_TrjA25TDGAFft577zEKZIyWwvdrCn56/view?usp=sharing
```

#### Setup Disaster Recovery :

```
1. EC2 Instances

    - Windows :
        - 30 GB 
    - Linux   :
        - 8 GB

2. RDS Databases
    - MySQL :
        - 20 GB 
    - Oracle
    - MSSQL

```